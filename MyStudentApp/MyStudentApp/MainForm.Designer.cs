﻿namespace MyStudentApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewStud = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddStudent = new System.Windows.Forms.Button();
            this.btnRemoveStudent = new System.Windows.Forms.Button();
            this.btnEditStudent = new System.Windows.Forms.Button();
            this.dataGridViewStudCourses = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEnroll = new System.Windows.Forms.Button();
            this.dataGridViewCourses = new System.Windows.Forms.DataGridView();
            this.dataGridViewCoursesStud = new System.Windows.Forms.DataGridView();
            this.btnAddCourse = new System.Windows.Forms.Button();
            this.btnRemoveCourse = new System.Windows.Forms.Button();
            this.btnEditCourse = new System.Windows.Forms.Button();
            this.btnGrade = new System.Windows.Forms.Button();
            this.btnRemoveEnrollment = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudCourses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoursesStud)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewStud
            // 
            this.dataGridViewStud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStud.Location = new System.Drawing.Point(29, 71);
            this.dataGridViewStud.Name = "dataGridViewStud";
            this.dataGridViewStud.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStud.Size = new System.Drawing.Size(360, 196);
            this.dataGridViewStud.TabIndex = 0;
            this.dataGridViewStud.SelectionChanged += new System.EventHandler(this.dataGridViewStud_SelectionChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Students";
            // 
            // btnAddStudent
            // 
            this.btnAddStudent.Location = new System.Drawing.Point(32, 289);
            this.btnAddStudent.Name = "btnAddStudent";
            this.btnAddStudent.Size = new System.Drawing.Size(75, 23);
            this.btnAddStudent.TabIndex = 2;
            this.btnAddStudent.Text = "Add Student";
            this.btnAddStudent.UseVisualStyleBackColor = true;
            // 
            // btnRemoveStudent
            // 
            this.btnRemoveStudent.Location = new System.Drawing.Point(149, 289);
            this.btnRemoveStudent.Name = "btnRemoveStudent";
            this.btnRemoveStudent.Size = new System.Drawing.Size(105, 23);
            this.btnRemoveStudent.TabIndex = 3;
            this.btnRemoveStudent.Text = "Remove Student";
            this.btnRemoveStudent.UseVisualStyleBackColor = true;
            // 
            // btnEditStudent
            // 
            this.btnEditStudent.Location = new System.Drawing.Point(305, 289);
            this.btnEditStudent.Name = "btnEditStudent";
            this.btnEditStudent.Size = new System.Drawing.Size(75, 23);
            this.btnEditStudent.TabIndex = 4;
            this.btnEditStudent.Text = "Edit Student";
            this.btnEditStudent.UseVisualStyleBackColor = true;
            // 
            // dataGridViewStudCourses
            // 
            this.dataGridViewStudCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudCourses.Location = new System.Drawing.Point(501, 71);
            this.dataGridViewStudCourses.Name = "dataGridViewStudCourses";
            this.dataGridViewStudCourses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStudCourses.Size = new System.Drawing.Size(412, 196);
            this.dataGridViewStudCourses.TabIndex = 5;
            this.dataGridViewStudCourses.SelectionChanged += new System.EventHandler(this.dataGridViewStudCourses_SelectionChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(512, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Courses enrolled by student";
            // 
            // btnEnroll
            // 
            this.btnEnroll.Location = new System.Drawing.Point(504, 289);
            this.btnEnroll.Name = "btnEnroll";
            this.btnEnroll.Size = new System.Drawing.Size(134, 23);
            this.btnEnroll.TabIndex = 7;
            this.btnEnroll.Text = "Enroll student in course";
            this.btnEnroll.UseVisualStyleBackColor = true;
            // 
            // dataGridViewCourses
            // 
            this.dataGridViewCourses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCourses.Location = new System.Drawing.Point(29, 352);
            this.dataGridViewCourses.Name = "dataGridViewCourses";
            this.dataGridViewCourses.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCourses.Size = new System.Drawing.Size(360, 196);
            this.dataGridViewCourses.TabIndex = 8;
            this.dataGridViewCourses.SelectionChanged += new System.EventHandler(this.dataGridViewCourses_SelectionChanged);
            // 
            // dataGridViewCoursesStud
            // 
            this.dataGridViewCoursesStud.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCoursesStud.Location = new System.Drawing.Point(501, 352);
            this.dataGridViewCoursesStud.Name = "dataGridViewCoursesStud";
            this.dataGridViewCoursesStud.Size = new System.Drawing.Size(412, 196);
            this.dataGridViewCoursesStud.TabIndex = 9;
            // 
            // btnAddCourse
            // 
            this.btnAddCourse.Location = new System.Drawing.Point(32, 558);
            this.btnAddCourse.Name = "btnAddCourse";
            this.btnAddCourse.Size = new System.Drawing.Size(75, 23);
            this.btnAddCourse.TabIndex = 10;
            this.btnAddCourse.Text = "Add Course";
            this.btnAddCourse.UseVisualStyleBackColor = true;
            // 
            // btnRemoveCourse
            // 
            this.btnRemoveCourse.Location = new System.Drawing.Point(149, 558);
            this.btnRemoveCourse.Name = "btnRemoveCourse";
            this.btnRemoveCourse.Size = new System.Drawing.Size(105, 23);
            this.btnRemoveCourse.TabIndex = 11;
            this.btnRemoveCourse.Text = "Remove Course";
            this.btnRemoveCourse.UseVisualStyleBackColor = true;
            // 
            // btnEditCourse
            // 
            this.btnEditCourse.Location = new System.Drawing.Point(305, 558);
            this.btnEditCourse.Name = "btnEditCourse";
            this.btnEditCourse.Size = new System.Drawing.Size(75, 23);
            this.btnEditCourse.TabIndex = 12;
            this.btnEditCourse.Text = "Edit Course";
            this.btnEditCourse.UseVisualStyleBackColor = true;
            // 
            // btnGrade
            // 
            this.btnGrade.Location = new System.Drawing.Point(504, 558);
            this.btnGrade.Name = "btnGrade";
            this.btnGrade.Size = new System.Drawing.Size(75, 23);
            this.btnGrade.TabIndex = 13;
            this.btnGrade.Text = "Grade Student";
            this.btnGrade.UseVisualStyleBackColor = true;
            // 
            // btnRemoveEnrollment
            // 
            this.btnRemoveEnrollment.Location = new System.Drawing.Point(617, 558);
            this.btnRemoveEnrollment.Name = "btnRemoveEnrollment";
            this.btnRemoveEnrollment.Size = new System.Drawing.Size(158, 23);
            this.btnRemoveEnrollment.TabIndex = 14;
            this.btnRemoveEnrollment.Text = "Remove Student From Course";
            this.btnRemoveEnrollment.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Courses";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(512, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Students enrolled in course";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 613);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRemoveEnrollment);
            this.Controls.Add(this.btnGrade);
            this.Controls.Add(this.btnEditCourse);
            this.Controls.Add(this.btnRemoveCourse);
            this.Controls.Add(this.btnAddCourse);
            this.Controls.Add(this.dataGridViewCoursesStud);
            this.Controls.Add(this.dataGridViewCourses);
            this.Controls.Add(this.btnEnroll);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridViewStudCourses);
            this.Controls.Add(this.btnEditStudent);
            this.Controls.Add(this.btnRemoveStudent);
            this.Controls.Add(this.btnAddStudent);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewStud);
            this.Name = "MainForm";
            this.Text = "Students Management Application";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudCourses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCourses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCoursesStud)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewStud;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddStudent;
        private System.Windows.Forms.Button btnRemoveStudent;
        private System.Windows.Forms.Button btnEditStudent;
        private System.Windows.Forms.DataGridView dataGridViewStudCourses;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnEnroll;
        private System.Windows.Forms.DataGridView dataGridViewCourses;
        private System.Windows.Forms.DataGridView dataGridViewCoursesStud;
        private System.Windows.Forms.Button btnAddCourse;
        private System.Windows.Forms.Button btnRemoveCourse;
        private System.Windows.Forms.Button btnEditCourse;
        private System.Windows.Forms.Button btnGrade;
        private System.Windows.Forms.Button btnRemoveEnrollment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

