﻿using MyStudentApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStudentApp
{
    public partial class CourseForm : Form
    {
        public Course FormCourse { get; set; }
        public CourseForm()
        {
            InitializeComponent();
        }
        private void CourseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FormCourse == null) FormCourse = new Course();
            FormCourse.Title = txtTitle.Text;
            FormCourse.Credits = (int)numCredits.Value;
        }
        private void CourseForm_Load(object sender, EventArgs e)
        {
            if (FormCourse != null)
            {
                txtTitle.Text = FormCourse.Title;
                numCredits.Value = FormCourse.Credits;
                lblID.Text = FormCourse.CourseID.ToString();
            }
        }
    }
}
