﻿using Microsoft.EntityFrameworkCore;
using MyStudentApp.Data;
using MyStudentApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStudentApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }


        // Initial load
        private void LoadStudents()
        {
            using (SchoolContext context = new SchoolContext())
            {
                dataGridViewStud.DataSource = context.Students.Select(s => new { s.ID, Name = $"{s.FirstMidName} {s.LastName}", s.EnrollmentDate }).ToList();
            }
        }

        private void LoadCourses()
        {
            using (SchoolContext context = new SchoolContext())
            {
                dataGridViewCourses.DataSource = context.Courses.Select(c => new { ID = c.CourseID, c.Title, c.Credits }).ToList();
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadStudents();
            LoadCourses();
        }

        // Fn to load data based on selection in students/courses

        private void LoadStudentCources(int studID)
        {
            using (SchoolContext context = new SchoolContext())
            {
                dataGridViewStudCourses.DataSource = context.Enrollments.Where(x => x.StudentID == studID).Include(z => z.Course).Select(c => new {
                    c.CourseID,
                    c.Course.Title,
                    c.Course.Credits,
                    Grade = (int?)c.Grade
                }).ToList();
            }
        }
        private void LoadCoursesStudents(int courseID)
        {
            using (SchoolContext context = new SchoolContext())
            {
                dataGridViewCoursesStud.DataSource = context.Enrollments.Where(x => x.CourseID == courseID).Include(z => z.Student).Select(c => new
                {
                    c.StudentID,
                    StudentName = $"{c.Student.FirstMidName} {c.Student.LastName}",
                    Grade = (int?)c.Grade
                }).ToList();
            }
        }

        private void dataGridViewStud_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewStud.SelectedRows.Count > 0)
            {
                int studID = (int)dataGridViewStud.SelectedRows[0].Cells["ID"].Value;
                LoadStudentCources(studID);
            }
        }

        private void dataGridViewStudCourses_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewCourses.SelectedRows.Count > 0)
            {
                int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                LoadCoursesStudents(courseID);
            }
        }

        private void dataGridViewCourses_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridViewCourses.SelectedRows.Count > 0)
            {
                int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                LoadCoursesStudents(courseID);
            }
        }

        // Buttons events

        // Students
        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            StudentForm frmStudent = new StudentForm();
            if (DialogResult.OK == frmStudent.ShowDialog())
            {
                using (SchoolContext context = new SchoolContext())
                {
                    Student s = new Student()
                    {
                        FirstMidName = frmStudent.FormStudent.FirstMidName,
                        LastName = frmStudent.FormStudent.LastName,
                        EnrollmentDate = frmStudent.FormStudent.EnrollmentDate
                    };
                    context.Students.Add(s);
                    context.SaveChanges();
                    LoadStudents();
                }
            }
        }

        private void btnRemoveStudent_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo))
            {
                if (dataGridViewStud.SelectedRows.Count > 0)
                {
                    int studID = (int)dataGridViewStud.SelectedRows[0].Cells["ID"].Value;
                    using (SchoolContext context = new SchoolContext())
                    {
                        context.Students.Remove(context.Students.First(x => x.ID == studID));
                        context.SaveChanges();
                    }
                    if (dataGridViewCourses.SelectedRows.Count > 0)
                    {
                        int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                        LoadCoursesStudents(courseID);
                    }
                    LoadStudents();
                }
            }
        }

        private void btnEditStudent_Click(object sender, EventArgs e)
        {
            if (dataGridViewStud.SelectedRows.Count > 0)
            {
                using (SchoolContext context = new SchoolContext())
                {
                    int studID = (int)dataGridViewStud.SelectedRows[0].Cells["ID"].Value;
                    StudentForm frmStudent = new StudentForm();
                    frmStudent.FormStudent = context.Students.First(x => x.ID == studID);
                    if (DialogResult.OK == frmStudent.ShowDialog())
                    {
                        context.SaveChanges();
                        LoadStudents();
                    }
                }
            }
        }

            // Courses
        private void btnAddCourse_Click(object sender, EventArgs e)
        {
            CourseForm frmCourse = new CourseForm();
            if (DialogResult.OK == frmCourse.ShowDialog())
            {
                using (SchoolContext context = new SchoolContext())
                {
                    Course c = new Course()
                    {
                        Title = frmCourse.FormCourse.Title,
                        Credits = frmCourse.FormCourse.Credits,
                    };
                    context.Courses.Add(c);
                    context.SaveChanges();
                    LoadCourses();
                }
            }
        }

        private void btnRemoveCourse_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Are you sure?", "Confirm", MessageBoxButtons.YesNo))
            {
                if (dataGridViewCourses.SelectedRows.Count > 0)
                {
                    int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                    using (SchoolContext context = new SchoolContext())
                    {
                        context.Courses.Remove(context.Courses.First(x => x.CourseID == courseID));
                        context.SaveChanges();
                    }
                    if (dataGridViewStud.SelectedRows.Count > 0)
                    {
                        int studID = (int)dataGridViewStud.SelectedRows[0].Cells["ID"].Value;
                        LoadStudentCources(studID);
                    }
                    LoadCourses();
                }
            }
        }

        private void btnEditCourse_Click(object sender, EventArgs e)
        {
            if (dataGridViewCourses.SelectedRows.Count > 0)
            {
                using (SchoolContext context = new SchoolContext())
                {
                    int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                    CourseForm frmStudent = new CourseForm();
                    frmStudent.FormCourse = context.Courses.First(x => x.CourseID == courseID);
                    if (DialogResult.OK == frmStudent.ShowDialog())
                    {
                        context.SaveChanges();
                        LoadCourses();
                    }
                }
            }
        }

            // Enrollment
        private void btnEnroll_Click(object sender, EventArgs e)
        {
            if (dataGridViewStud.SelectedRows.Count > 0 && dataGridViewCourses.SelectedRows.Count > 0)
            {
                int studID = (int)dataGridViewStud.SelectedRows[0].Cells["ID"].Value;
                int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                using (SchoolContext context = new SchoolContext())
                {
                    if (context.Enrollments.Any(x => x.CourseID == courseID && x.StudentID == studID))
                    {
                        MessageBox.Show("Student is already enrolled in this course.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                    Enrollment newEnr = new Enrollment();
                    newEnr.StudentID = studID;
                    newEnr.CourseID = courseID;
                    context.Enrollments.Add(newEnr);
                    context.SaveChanges();
                }
                LoadCoursesStudents(courseID);
                LoadStudentCources(studID);
            }
        }

            // Grade / Remove enrollment
        private void btnGrade_Click(object sender, EventArgs e)
        {
            if (dataGridViewCoursesStud.SelectedRows.Count > 0 && dataGridViewCourses.SelectedRows.Count > 0)
            {
                int studID = (int)dataGridViewCoursesStud.SelectedRows[0].Cells["StudentID"].Value;
                int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                using (SchoolContext context = new SchoolContext())
                {
                    var enrollment = context.Enrollments.Include(y => y.Student).First(x => x.CourseID == courseID && x.StudentID == studID);
                    GradeStudentForm frmGrade = new GradeStudentForm();
                    frmGrade.StudentName = $"{enrollment.Student.FirstMidName} {enrollment.Student.LastName}";
                    frmGrade.Grade = (int?)enrollment.Grade ?? 6;
                    if (DialogResult.OK == frmGrade.ShowDialog())
                    {
                        enrollment.Grade = (Grade)frmGrade.Grade;
                        context.SaveChanges();
                        LoadCoursesStudents(courseID);
                        LoadStudentCources(studID);
                    }
                }
            }
        }

        private void btnRemoveEnrollment_Click(object sender, EventArgs e)
        {
            if (dataGridViewCoursesStud.SelectedRows.Count > 0 && dataGridViewCourses.SelectedRows.Count > 0)
            {
                int studID = (int)dataGridViewCoursesStud.SelectedRows[0].Cells["StudentID"].Value;
                int courseID = (int)dataGridViewCourses.SelectedRows[0].Cells["ID"].Value;
                using (SchoolContext context = new SchoolContext())
                {
                    var enrollment = context.Enrollments.Include(y => y.Student).First(x => x.CourseID == courseID && x.StudentID == studID);
                    context.Enrollments.Remove(enrollment);
                    context.SaveChanges();
                    LoadCoursesStudents(courseID);
                    LoadStudentCources(studID);
                }
            }
        }
    }
}
