﻿using MyStudentApp.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStudentApp
{
    public partial class StudentForm : Form
    {
        public Student FormStudent { get; set; }
        public StudentForm()
        {
            InitializeComponent();
        }

        private void StudentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FormStudent == null) FormStudent = new Student();
            FormStudent.FirstMidName = txtFirstMiddleName.Text;
            FormStudent.LastName = txtLastName.Text;
            FormStudent.EnrollmentDate = dateTimeEnroll.Value.Date;
        }

        private void StudentForm_Load(object sender, EventArgs e)
        {
            if (FormStudent != null)
            {
                lblID.Text = FormStudent.ID.ToString();
                txtFirstMiddleName.Text = FormStudent.FirstMidName;
                txtLastName.Text = FormStudent.LastName;
                dateTimeEnroll.Value = FormStudent.EnrollmentDate;
            }
            else
            {
                dateTimeEnroll.Value = DateTime.Now.Date;
            }
        }
    }
}
