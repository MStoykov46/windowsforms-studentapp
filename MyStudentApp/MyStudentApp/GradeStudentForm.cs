﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyStudentApp
{
    public partial class GradeStudentForm : Form
    {
        public string StudentName { get; set; }
        public int Grade { get; set; }
        public GradeStudentForm()
        {
            InitializeComponent();
        }
        private void GradeStudentForm_Load(object sender, EventArgs e)
        {
            numGrade.Value = Grade;
            lblName.Text = StudentName;
        }
        private void GradeStudentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Grade = (int)numGrade.Value;
        }

    }
}
